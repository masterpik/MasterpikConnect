package com.masterpik.connect;

import com.masterpik.connect.enums.Statistics;
import com.masterpik.database.db.Connect;
import com.masterpik.messages.Api;

import java.sql.Connection;

public class MasterpikConnect {

    public static boolean isSpigot;
    public static Connection dbCo;

    public static String masterpikChat;
    public static String rushChat;
    public static String towerChat;
    public static String jumpsrunChat;

    public static void onEnable() {
        dbCo = Connect.getConnection();

        Statistics.initMessages();

        masterpikChat = Api.getString("general.masterpik.main.logoTextChat", isSpigot)+" ";
        rushChat = Api.getString("rush.chat.mainPrefix", isSpigot);
        towerChat = Api.getString("tower.chat.mainPrefix", isSpigot);
        jumpsrunChat = Api.getString("jumpsrun.chat.mainPrefix", isSpigot);


    }

}

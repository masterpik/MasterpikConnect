package com.masterpik.connect.bungee.commands;

import com.masterpik.api.json.ChatText;
import com.masterpik.api.json.HoverEvent;
import com.masterpik.api.json.HoverEvents;
import com.masterpik.api.texts.Colors;
import com.masterpik.api.util.UtilJson;
import com.masterpik.bungeeconnect.database.UUIDName;
import com.masterpik.connect.MasterpikConnect;
import com.masterpik.connect.api.PlayersStatistics;
import com.masterpik.connect.enums.Statistics;
import com.masterpik.connect.enums.StatsPlayers;
import com.masterpik.connect.enums.Times;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.protocol.packet.Chat;

import java.util.UUID;

public class CommandsPlayersStatistics extends Command {

    public static String generalStatsMsg;

    public CommandsPlayersStatistics(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {

        if (sender instanceof ProxiedPlayer) {

            ProxiedPlayer player = (ProxiedPlayer) sender;
            UUID uuid = null;

            if (args == null
                    || args.length == 0) {
                uuid = player.getUniqueId();
            } else {
                uuid = UUIDName.playerNameToUUID(args[0]);
            }

            if (uuid == null) {
                uuid = player.getUniqueId();
            }

            if (uuid != null) {

                String name = UUIDName.uuidToPlayerName(uuid);

                ChatText generalStats = new ChatText("\n" + MasterpikConnect.masterpikChat+generalStatsMsg.replaceAll("%v", name)).
                        setHoverEvent(new HoverEvent(HoverEvents.SHOW_TEXT,
                                new ChatText[] {
                                        new ChatText(MasterpikConnect.masterpikChat),
                                        new ChatText(" "+generalStatsMsg.replaceAll("%v", name)+"\n\n\n"),
                                        new ChatText("§6§l➤ "+Statistics.PIK.getGeneralMessage().replaceAll("%v", Integer.toString(PlayersStatistics.getStatistic(StatsPlayers.GENERAL, uuid, Statistics.PIK)))+"\n\n"),
                                        new ChatText("§6§l➤ "+Statistics.SPENT_TIME.getGeneralMessage().replaceAll("%v", Times.getTime(PlayersStatistics.getStatistic(StatsPlayers.GENERAL, uuid, Statistics.SPENT_TIME)))+"\n")}));

                player.unsafe().sendPacket(new Chat(UtilJson.getJsonFromObject(new Object[]{"", generalStats})));

                ChatText rushStats = new ChatText(MasterpikConnect.rushChat).
                        setHoverEvent(new HoverEvent(HoverEvents.SHOW_TEXT,
                                new ChatText[] {
                                        new ChatText(MasterpikConnect.rushChat),
                                        new ChatText(" "+generalStatsMsg.replaceAll("%v", name)+"\n\n\n"),
                                        new ChatText("§6§l➤ "+Statistics.PIK.getGeneralMessage().replaceAll("%v", Integer.toString(PlayersStatistics.getStatistic(StatsPlayers.RUSH, uuid, Statistics.PIK)))+"\n\n"),
                                        new ChatText("§6§l➤ "+Statistics.SPENT_TIME.getGeneralMessage().replaceAll("%v", Times.getTime(PlayersStatistics.getStatistic(StatsPlayers.RUSH, uuid, Statistics.SPENT_TIME)))+"\n\n"),
                                        new ChatText("§6§l➤ "+Statistics.GAMES_AMOUNT.getGeneralMessage().replaceAll("%v", Integer.toString(PlayersStatistics.getStatistic(StatsPlayers.RUSH, uuid, Statistics.GAMES_AMOUNT)))+"\n\n"),
                                        new ChatText("§6§l➤ "+Statistics.WIN_AMOUNT.getGeneralMessage().replaceAll("%v", Integer.toString(PlayersStatistics.getStatistic(StatsPlayers.RUSH, uuid, Statistics.WIN_AMOUNT)))+"\n\n"),
                                        new ChatText("§6§l➤ "+Statistics.LOST_AMOUNT.getGeneralMessage().replaceAll("%v", Integer.toString(PlayersStatistics.getStatistic(StatsPlayers.RUSH, uuid, Statistics.LOST_AMOUNT)))+"\n\n"),
                                        new ChatText("§6§l➤ "+Statistics.KILL_AMOUNT.getGeneralMessage().replaceAll("%v", Integer.toString(PlayersStatistics.getStatistic(StatsPlayers.RUSH, uuid, Statistics.KILL_AMOUNT)))+"\n\n"),
                                        new ChatText("§6§l➤ "+Statistics.DEATH_AMOUNT.getGeneralMessage().replaceAll("%v", Integer.toString(PlayersStatistics.getStatistic(StatsPlayers.RUSH, uuid, Statistics.DEATH_AMOUNT))+"\n\n")),
                                        new ChatText("§6§l➤ "+Statistics.BED_BREAK_AMOUNT.getGeneralMessage().replaceAll("%v", Integer.toString(PlayersStatistics.getStatistic(StatsPlayers.RUSH, uuid, Statistics.BED_BREAK_AMOUNT)))+"\n")}));


                ChatText towerStats = new ChatText(MasterpikConnect.towerChat).
                        setHoverEvent(new HoverEvent(HoverEvents.SHOW_TEXT,
                                new ChatText[] {
                                        new ChatText(MasterpikConnect.towerChat),
                                        new ChatText(" "+generalStatsMsg.replaceAll("%v", name)+"\n\n\n"),
                                        new ChatText("§6§l➤ "+Statistics.PIK.getGeneralMessage().replaceAll("%v", Integer.toString(PlayersStatistics.getStatistic(StatsPlayers.TOWER, uuid, Statistics.PIK)))+"\n\n"),
                                        new ChatText("§6§l➤ "+Statistics.SPENT_TIME.getGeneralMessage().replaceAll("%v", Times.getTime(PlayersStatistics.getStatistic(StatsPlayers.TOWER, uuid, Statistics.SPENT_TIME)))+"\n\n"),
                                        new ChatText("§6§l➤ "+Statistics.GAMES_AMOUNT.getGeneralMessage().replaceAll("%v", Integer.toString(PlayersStatistics.getStatistic(StatsPlayers.TOWER, uuid, Statistics.GAMES_AMOUNT)))+"\n\n"),
                                        new ChatText("§6§l➤ "+Statistics.WIN_AMOUNT.getGeneralMessage().replaceAll("%v", Integer.toString(PlayersStatistics.getStatistic(StatsPlayers.TOWER, uuid, Statistics.WIN_AMOUNT)))+"\n\n"),
                                        new ChatText("§6§l➤ "+Statistics.LOST_AMOUNT.getGeneralMessage().replaceAll("%v", Integer.toString(PlayersStatistics.getStatistic(StatsPlayers.TOWER, uuid, Statistics.LOST_AMOUNT)))+"\n\n"),
                                        new ChatText("§6§l➤ "+Statistics.KILL_AMOUNT.getGeneralMessage().replaceAll("%v", Integer.toString(PlayersStatistics.getStatistic(StatsPlayers.TOWER, uuid, Statistics.KILL_AMOUNT)))+"\n\n"),
                                        new ChatText("§6§l➤ "+Statistics.DEATH_AMOUNT.getGeneralMessage().replaceAll("%v", Integer.toString(PlayersStatistics.getStatistic(StatsPlayers.TOWER, uuid, Statistics.DEATH_AMOUNT))+"\n\n")),
                                        new ChatText("§6§l➤ "+Statistics.POINT_MARK_AMOUNT.getGeneralMessage().replaceAll("%v", Integer.toString(PlayersStatistics.getStatistic(StatsPlayers.TOWER, uuid, Statistics.POINT_MARK_AMOUNT)))+"\n")}));

                ChatText jumpsrunStats = new ChatText(MasterpikConnect.jumpsrunChat).
                        setHoverEvent(new HoverEvent(HoverEvents.SHOW_TEXT,
                                new ChatText[] {
                                        new ChatText(MasterpikConnect.jumpsrunChat),
                                        new ChatText(" "+generalStatsMsg.replaceAll("%v", name)+"\n\n\n"),
                                        new ChatText("§6§l➤ "+Statistics.PIK.getGeneralMessage().replaceAll("%v", Integer.toString(PlayersStatistics.getStatistic(StatsPlayers.JUMPSRUN, uuid, Statistics.PIK)))+"\n\n"),
                                        new ChatText("§6§l➤ "+Statistics.SPENT_TIME.getGeneralMessage().replaceAll("%v", Times.getTime(PlayersStatistics.getStatistic(StatsPlayers.JUMPSRUN, uuid, Statistics.SPENT_TIME)))+"\n\n"),
                                        new ChatText("§6§l➤ "+Statistics.GAMES_AMOUNT.getGeneralMessage().replaceAll("%v", Integer.toString(PlayersStatistics.getStatistic(StatsPlayers.JUMPSRUN, uuid, Statistics.GAMES_AMOUNT)))+"\n\n"),
                                        new ChatText("§6§l➤ "+Statistics.WIN_AMOUNT.getGeneralMessage().replaceAll("%v", Integer.toString(PlayersStatistics.getStatistic(StatsPlayers.JUMPSRUN, uuid, Statistics.WIN_AMOUNT)))+"\n\n"),
                                        new ChatText("§6§l➤ "+Statistics.LOST_AMOUNT.getGeneralMessage().replaceAll("%v", Integer.toString(PlayersStatistics.getStatistic(StatsPlayers.JUMPSRUN, uuid, Statistics.LOST_AMOUNT)))+"\n")}));

                player.unsafe().sendPacket(new Chat(UtilJson.getJsonFromObject(new Object[]{"", rushStats, towerStats, jumpsrunStats})));
                player.unsafe().sendPacket(new Chat(UtilJson.getJsonFromObject(new Object[]{"", new ChatText("(passe ta souris sur les différents jeux)\n ").setColor(Colors.LIGHT_PURPLE).setItalic(true)})));


            }

        }

    }
}

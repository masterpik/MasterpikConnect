package com.masterpik.connect.bungee.commands;

import com.masterpik.api.json.ChatText;
import com.masterpik.api.util.UtilJson;
import com.masterpik.bungeeconnect.database.UUIDName;
import com.masterpik.connect.MasterpikConnect;
import com.masterpik.connect.api.PlayersStatistics;
import com.masterpik.connect.enums.Statistics;
import com.masterpik.connect.enums.StatsPlayers;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.protocol.packet.Chat;

import java.util.UUID;

public class CommandsPlayersPiksPoints extends Command {
    public CommandsPlayersPiksPoints(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {

        if (sender instanceof ProxiedPlayer) {

            ProxiedPlayer player = (ProxiedPlayer) sender;
            UUID uuid = null;

            if (args == null
                    || args.length == 0) {
                uuid = player.getUniqueId();
            } else {
                uuid = UUIDName.playerNameToUUID(args[0]);
            }

            if (uuid == null) {
                uuid = player.getUniqueId();
            }

            if (uuid != null) {
                player.unsafe().sendPacket(new Chat(UtilJson.getJsonFromObject(new Object[]{"", new ChatText("\n" + MasterpikConnect.masterpikChat), new ChatText(Statistics.PIK.getMessage().replaceAll("%v", UUIDName.uuidToPlayerName(uuid)))})));
                player.unsafe().sendPacket(new Chat(UtilJson.getJsonFromObject(new Object[]{"", new ChatText("" + MasterpikConnect.rushChat + "§d§l➤ §r§a§l" + Integer.toString(PlayersStatistics.getStatistic(StatsPlayers.RUSH, uuid, Statistics.PIK)))})));
                player.unsafe().sendPacket(new Chat(UtilJson.getJsonFromObject(new Object[]{"", new ChatText("" + MasterpikConnect.towerChat + "§d§l➤ §r§a§l" + Integer.toString(PlayersStatistics.getStatistic(StatsPlayers.TOWER, uuid, Statistics.PIK)))})));
                player.unsafe().sendPacket(new Chat(UtilJson.getJsonFromObject(new Object[]{"", new ChatText("" + MasterpikConnect.jumpsrunChat + "§d§l➤ §r§a§l" + Integer.toString(PlayersStatistics.getStatistic(StatsPlayers.JUMPSRUN, uuid, Statistics.PIK)))})));
                player.unsafe().sendPacket(new Chat(UtilJson.getJsonFromObject(new Object[]{"", new ChatText("§0§l▌§r§9§lTOTAL§0§l▌§r §d§l➤ §r§b§l" + Integer.toString(PlayersStatistics.getStatistic(StatsPlayers.GENERAL, uuid, Statistics.PIK)) + "\n ")})));
                //player.unsafe().sendPacket(new Chat(UtilJson.getJsonFromObject(new Object[]{"", new ChatText("§d§l➤ §r§5"+Integer.toString(PlayersStatistics.getStatistic(StatsPlayers.GENERAL, uuid, Statistics.PIK))+" §9§lTOTAL")})));
                //player.unsafe().sendPacket(new Chat(UtilJson.getJsonFromObject(new Object[]{"", new ChatText("\n"+MasterpikConnect.masterpikChat), new ChatText(Statistics.PIK.getMessage().replaceAll("%v", Integer.toString(PlayersStatistics.getStatistic(StatsPlayers.GENERAL, uuid, Statistics.PIK))))})));
            }

        }

    }
}

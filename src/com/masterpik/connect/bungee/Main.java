package com.masterpik.connect.bungee;

import com.masterpik.bungeecommands.aliases.Aliases;
import com.masterpik.connect.MasterpikConnect;
import com.masterpik.connect.bungee.commands.CommandsPlayersPiksPoints;
import com.masterpik.connect.bungee.commands.CommandsPlayersStatistics;
import com.masterpik.messages.Api;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.api.plugin.PluginManager;

import java.util.ArrayList;

public class Main extends Plugin {

    public void onEnable() {
        MasterpikConnect.isSpigot = false;
        MasterpikConnect.onEnable();

        CommandsPlayersStatistics.generalStatsMsg = Api.getString("stats.general.stats", MasterpikConnect.isSpigot);





        PluginManager pluginManager = ProxyServer.getInstance().getPluginManager();

        pluginManager.registerCommand(this, new CommandsPlayersPiksPoints("pik"));
        Aliases.addCommand("pik", new ArrayList<>());


        String name = "stats";

        ArrayList<String> alias = new ArrayList<>();
        alias.add("stat");
        alias.add("statistiques");
        alias.add("statistique");
        alias.add("statistics");
        alias.add("statistic");
        alias.add(name);
        for (String cmd : alias) {
            pluginManager.registerCommand(this, new CommandsPlayersStatistics(cmd));
        }
        alias.remove(name);

        Aliases.addCommand(name, alias);


    }

}

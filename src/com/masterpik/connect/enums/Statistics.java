package com.masterpik.connect.enums;

import com.masterpik.connect.MasterpikConnect;
import com.masterpik.messages.Api;

public enum Statistics {
    PIK("pik"),
    SPENT_TIME("spent_time"),
    GAMES_AMOUNT("games_amount"),
    WIN_AMOUNT("win_amount"),
    LOST_AMOUNT("lost_amount"),
    KILL_AMOUNT("kill_amount"),
    DEATH_AMOUNT("death_amount"),
    BED_BREAK_AMOUNT("bed_break_amount", StatsPlayers.RUSH),
    POINT_MARK_AMOUNT("point_mark_amount", StatsPlayers.TOWER),
    JUMPS_AMOUNT("jumps_amount", StatsPlayers.JUMPSRUN),
    FALL_AMOUNT("fall_amount", StatsPlayers.JUMPSRUN);


    private String link;
    private StatsPlayers dependence;
    private String message;
    private String generalMessage;

    Statistics(String link, StatsPlayers dependence) {
        this.link = link;
        this.dependence = dependence;
    }

    Statistics(String link) {
        this.link = link;
        this.dependence = null;
    }

    public String getLink() {
        return link;
    }

    public StatsPlayers getDependence() {
        return dependence;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getGeneralMessage() {
        return generalMessage;
    }

    public void setGeneralMessage(String generalMessage) {
        this.generalMessage = generalMessage;
    }

    public static void initMessages() {

        for (Statistics st : Statistics.values()) {
            String msg = Api.getString("stats." + st.getLink(), MasterpikConnect.isSpigot, false);
            if (msg != null) {
                st.setMessage(msg);
                st.setGeneralMessage(msg);
            }
            String gMsg = Api.getString("stats.general." + st.getLink(), MasterpikConnect.isSpigot, false);
            if (gMsg != null) {
                st.setGeneralMessage(gMsg);
            }
        }

    }

    @Override
    public String toString() {
        return "Statistics{" +
                "link='" + link + '\'' +
                ", dependence=" + dependence +
                '}';
    }
}

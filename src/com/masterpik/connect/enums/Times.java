package com.masterpik.connect.enums;

import com.masterpik.connect.MasterpikConnect;
import com.masterpik.messages.Api;

public enum Times {
    MINUTES(),
    HOURS(),
    DAYS();

    private String message;

    Times() {
        this.message = Api.getString("stats.times."+this.name().toLowerCase(), MasterpikConnect.isSpigot);
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "Times{" +
                "message='" + message + '\'' +
                '}';
    }

    public static String getTime(int minutes) {
        String re = "";

        int rest = minutes;

        try {

            int antiInf = 0;

            while (rest > 0 && antiInf < 40) {

                if (rest >= 1440) {

                    int days = (int) Math.floor(((double) rest) / 1440.0D);
                    re = re + DAYS.getMessage().replaceAll("%v", Integer.toString(days));
                    rest = rest - (days * 1440);

                    if (days > 1) {
                        re = re + "s";
                    }

                } else if (rest >= 60) {

                    int hours = (int) Math.floor(((double)rest) / 60.0D);
                    re = re + HOURS.getMessage().replaceAll("%v", Integer.toString(hours));
                    rest = rest - (hours * 60);

                    if (hours > 1) {
                        re = re + "s";
                    }

                } else {

                    re = re + MINUTES.getMessage().replaceAll("%v", Integer.toString(rest));

                    if (rest > 1) {
                        re = re + "s";
                    }

                    rest = rest - rest;

                }

                antiInf ++;
            }

        } catch (NumberFormatException e) {
            re = re;
        }

        return re;
    }
}

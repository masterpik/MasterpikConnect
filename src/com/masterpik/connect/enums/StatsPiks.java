package com.masterpik.connect.enums;

public enum StatsPiks {
    PARTY(10),
    RUSH(2),
    TOWER(4),
    JUMPSRUN(1),
    RUSH_AIR(1),
    RUSH_MOUNTAIN(3),
    RUSH_WATER(2),
    JUMPSRUN_GLASS(1),
    JUMPSRUN_ICE(2),
    JUMPSRUN_FLOWER(4),
    JUMPSRUN_STICK(6),
    JUMPSRUN_LVL_1(1),
    JUMPSRUN_LVL_2(2),
    JUMPSRUN_LVL_3(3);

    private int piks;

    StatsPiks(int piks) {
        this.piks = piks;
    }

    public int getPiks() {
        return piks;
    }

    @Override
    public String toString() {
        return "StatsPiks{" +
                "piks=" + piks +
                '}';
    }
}

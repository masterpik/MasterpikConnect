package com.masterpik.connect.enums;

public enum StatsPlayers {
    GENERAL("masterpik.stats_players_general"),
    RUSH("masterpik.stats_players_rush"),
    TOWER("masterpik.stats_players_tower"),
    JUMPSRUN("masterpik.stats_players_jumpsrun");

    private String link;

    StatsPlayers(String link) {
        this.link = link;
    }

    public String getLink() {
        return link;
    }

    @Override
    public String toString() {
        return "StatsPlayers{" +
                "link='" + link + '\'' +
                '}';
    }
}

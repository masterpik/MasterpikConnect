package com.masterpik.connect.api;

import com.masterpik.api.util.UtilDate;
import com.masterpik.connect.MasterpikConnect;
import com.masterpik.connect.enums.StatsPiks;
import com.masterpik.database.api.Query;
import com.masterpik.connect.enums.Statistics;
import com.masterpik.connect.enums.StatsPlayers;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.UUID;

public class PlayersStatistics {

    public static boolean playerConnexion(UUID uuid) {

        String query = "";
        ResultSet result = Query.queryOutpout(MasterpikConnect.dbCo, query, false);

        boolean firstConnect = true;

        if (result != null) {
            try {
                firstConnect = !result.next();
            } catch (SQLException e) {
                e.printStackTrace();
                return false;
            }
        }

        if (firstConnect) {
            boolean allTrue = true;
            for (StatsPlayers link : StatsPlayers.values()) {
                if (!insertPlayer(link, uuid)) {
                    allTrue = false;
                }
            }
            return allTrue;
        } else {
            return insertPlayer(StatsPlayers.GENERAL, uuid);
        }
    }

    public static boolean insertPlayer(StatsPlayers link, UUID uuid) {
        String query = "INSERT INTO "+link.getLink()+" (uuid) VALUES ('"+uuid.toString()+"')";
        return Query.queryInput(MasterpikConnect.dbCo, query);
    }



    public static boolean addSpentTime(UUID uuid, long milis, StatsPlayers link) {
        return addSpentTime(uuid, UtilDate.millisecondsToMinutes(milis), link);
    }
    public static boolean addSpentTime(UUID uuid, int time, StatsPlayers link) {
        boolean ok = true;

        if (!incrementStatistic(StatsPlayers.GENERAL, uuid, Statistics.SPENT_TIME, time)) {
            ok = false;
        }

        if (!incrementStatistic(link, uuid, Statistics.SPENT_TIME, time)) {
            ok = false;
        }

        return ok;
    }

    public static int getPikPointsWin(int piks, boolean isWin) {
        if (isWin) {
            return piks;
        } else {
            return piks/3;
        }
    }

    public static boolean addPikPoint(UUID uuid, Integer piks, StatsPlayers link, boolean isWinner) {
        return addPikPoint(uuid, getPikPointsWin(piks, isWinner), link);
    }
    public static boolean addPikPoint(UUID uuid, Integer piks, StatsPlayers link) {
        boolean ok = true;

        if (!incrementStatistic(StatsPlayers.GENERAL, uuid, Statistics.PIK, piks)) {
            ok = false;
        }

        if (!incrementStatistic(link, uuid, Statistics.PIK, piks)) {
            ok = false;
        }

        return ok;
    }




    public static Integer getStatistic(StatsPlayers link, UUID uuid, Statistics stat) {

        if (stat.getDependence() != null) {
            link = stat.getDependence();
        } else if (link == null) {
            return -1;
        }


        String query1 = "SELECT "+stat.getLink()+" FROM "+link.getLink()+" WHERE uuid = '"+uuid.toString()+"'";
        ResultSet result1 = Query.queryOutpout(MasterpikConnect.dbCo, query1);

        try {
            result1.next();
            return result1.getInt(stat.getLink());
            //while(result1.next()) {
            //}
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }

    public static boolean incrementStatistic(StatsPlayers link, UUID uuid, Statistics stat) {
        return incrementStatistic(link, uuid, stat, 1);
    }
    public static boolean incrementStatistic(StatsPlayers link, UUID uuid, Statistics stat, int inc) {

        if (stat.getDependence() != null) {
            link = stat.getDependence();
        } else if (link == null) {
            return false;
        }

        int old = getStatistic(link, uuid, stat);
        if (old == -1){
            return false;
        }
        int newInt = inc + old;


        String query = "UPDATE "+link.getLink()+" SET "+stat.getLink()+" = "+Integer.toString(newInt)+" WHERE uuid = '"+uuid.toString()+"'";

        return Query.queryInput(MasterpikConnect.dbCo, query);
    }

}
